#!/usr/bin/env python3

# -----------------------------
# (C) Sebastian Will, 2018
#
# This file is part of the InfraRed source code.
#
# InfraRed provides a generic framework for tree decomposition-based
# Boltzmann sampling over constraint networks
#

###############################
## @file
## Redprint v2.1 based on InfraRed
##
## Redprint provides Boltzmann sampling of sequences targeting
## multiple RNA structures. This file could be used as redprint
## module or redprint command line tool.
##
## @note Dependencies: the redprint tool needs ViennaRNA's RNA Python
## module, which could require to set the Python path like: export
## PYTHONPATH=$VRNAVIENNA_HOME/lib/python3.6/site-packages. For
## further dependencies see Infrared/treedecomp.py.

import random
import argparse
import itertools
import os
import sys
import abc

import infrared as ir
import infrared.rna as rna
import treedecomp

import RNA

############################################################
## Redprint Library

def design_model(targets, stacking=False):
    seqlen = len(targets[0])

    model = ir.Model()

    # one variable X_i per position i;
    # the value of X_i encodes the nucleotide at position i
    model.add_variables( seqlen, 4 )


    for i,target in enumerate(targets):
        bps = rna.parse(target)

        model.add_constraints( rna.BPComp( i, j ) for ( i, j ) in bps )

        if stacking:
            structureset = set(bps)
            model.add_functions(
                [rna.StackEnergy(i, j)
                    for (i, j) in bps
                    if (i+1, j-1) in structureset],
                f'energy{i}' )
        else:
            model.add_functions(
                [rna.BPEnergy(i, j, False)
                    for (i, j) in bps ], group = f'energy{i}')

        model.add_feature( f'E{i}', # feature name
                           f'energy{i}', # controlled group(s)
                           #
                           # function to evaluate the feature for a sample;
                           # NOTE how we have to bind i
                           lambda sample, i=i: RNA.energy_of_struct( rna.values_to_seq( sample.values() ),
                                                  targets[i] )
                         )

    model.add_functions( [ rna.GCCont( i = i ) for i in range(seqlen) ], group = 'gc' )
    model.add_feature( 'GC', 'gc', lambda sample, model=model, seqlen=seqlen: 100*model.eval_feature(sample, 'gc')/seqlen )

    return model

# END Redprint module
# ##########################################################

# ##########################################################
# main
#

## @brief command line tool definition
#  @param args command line arguments
def main(args):

    if args.listtds:
        print("Avalaible tree decomposition methods", treedecomp.get_td_factory_descriptors())
        return

    # init seed
    if args.seed == None:
        ir.seed(random.randint(0,2**31))
    else:
        ir.seed(args.seed)

    rna.set_bpenergy_table()
    rna.set_stacking_energy_table()

    # read instance
    with open(args.infile) as infh:
        structures = rna.read_inp(infh)

    weights=args.weight
    if weights is None: weights=[0]
    if len(weights) < len(structures):
        weights.extend([weights[-1]]*(len(structures)-len(weights)))

    mdbs = False

    # automatically turn on mdbs if any targets or tolerances are given
    if not args.nomdbs and (args.target is not None or args.gctarget is not None):
        mdbs = True

    targets=args.target
    if targets is not None:
        if len(targets) < len(structures):
            targets.extend([targets[-1]]*(len(structures)-len(targets)))

    tolerances=args.tolerance
    if tolerances is None: tolerances=[1]
    if len(tolerances) < len(structures):
        tolerances.extend([tolerances[-1]]*(len(structures)-len(tolerances)))

    if len(structures) == 0:
        print("At least one structure is required in input")
        exit(-1)

    seqlen = len(structures[0])

    td_factory = treedecomp.td_factory_from_descriptor(args.td)
    if td_factory is None:
        sys.stderr.write("[ERROR] Invalid tree decomposition method: "+args.td+"\n")
        exit(-1)

    model = design_model(structures, stacking=args.stacking)

    # set weights
    for i,weight in enumerate(weights):
        model.set_feature_weight(weight,f'E{i}')
    model.set_feature_weight(args.gcweight, 'GC')
    
    if args.write_dependency_graph:
        model.write_graph('dependency_graph.dot', True)
        ir.dotfile_to_pdf('dependency_graph.dot')

    # generate sampler
    sampler = ir.Sampler(model, td_factory=td_factory)

    # set targets
    if targets is not None:
        for i,(target,tolerance) in enumerate(zip(targets,tolerances)):
            if target is not None:
                sampler.set_target(target,tolerance,f'E{i}')

    if args.gctarget is not None:
        sampler.set_target(args.gctarget,args.gctolerance, 'GC')


    ## optionally, write tree decomposition
    if args.plot_td:
        sampler.plot_td("treedecomp.pdf")

    if args.verbose:
        print("Treewidth:",sampler.treewidth())

    if args.dry:
        return

    fstats = ir.FeatureStatistics()

    ## sample

    if mdbs:
        sample_generator = sampler.targeted_samples()
    else:
        sample_generator = sampler.samples()

    the_features = [f"E{i}" for i,_ in enumerate(structures)] + ["GC"]
    the_features = {fid:model.features[fid] for fid in the_features}

    try:
        sample_count = 0
        for sample in sample_generator:
            seq = rna.ass_to_seq(sample)

            print(seq, end='')

            if not args.seqonly:
                feature_values = {fid:model.eval_feature(sample, fid) for fid in the_features}
                fstats.record_features( the_features, feature_values )

                for i,struc in enumerate(structures):
                    e_val = feature_values[f"E{i}"]
                    print(f" E{i}={e_val:3.2f}", end='')

                gc_val = feature_values["GC"]
                print(f" GC={gc_val:3.2f}", end='')

            if args.checkvalid:
                for i,struc in enumerate(structures):
                    if not rna.is_valid(seq, struc):
                        print(" INVALID{}: {}".format(i,str(rna.invalid_bps(seq,struc))),end='')
            print()
            sample_count += 1
            if sample_count >= args.number:
                break
    except ir.ConsistencyError:
        sys.stderr.write("Inconsistent targets.\n")

    if args.verbose and not args.seqonly:
        if not fstats.empty():
            print("----------")
            print("Summary: ",end='')
        print(fstats.report())

if __name__ == "__main__":
    ## command line argument parser
    parser = argparse.ArgumentParser(description="Boltzmann sampling for RNA design with multiple target structures")
    parser.add_argument('infile', help="Input file")
    parser.add_argument('--td', type=str, default="nx",
                        help="Method for tree decomposition (see --listtds)")
    parser.add_argument('--listtds', action="store_true", help="List available tree decomposition methods")
    parser.add_argument('--stacking', action="store_true",
                        help="Use stacking energy model for sampling (default bp model)")

    parser.add_argument('-n','--number', type=int, default=10, help="Number of samples")
    parser.add_argument('--seed', type=int, default=None,
                        help="Seed infrared's random number generator (def=auto)")

    parser.add_argument('-v','--verbose', action="store_true", help="Verbose")
    parser.add_argument('--dry', action="store_true",
                        help="Dry run. Skip precomputation and sampling."
                        " For example, use with --verbose, --plot_td.")

    parser.add_argument('--seqonly', action="store_true",
                        help="Report only sequences (ommit energies and GC contents)")
    parser.add_argument('--checkvalid', action="store_true",
                        help="Check base pair complementarity for each structure and for each sample")

    parser.add_argument('--gcweight', type=float, default=0, help="GC weight")
    parser.add_argument('--weight', type=float, action="append",
                        help="Energy weight (def=0; in case, use last weight for remaining structures)")

    parser.add_argument('--nomdbs', action="store_true",
                        help="Don't perform multi-dim Boltzmann sampling to aim at targets; even if targets are given")

    parser.add_argument('--gctarget', type=float, default=None, help="GC target")

    parser.add_argument('--target', type=float, action="append",
                        help="Energy target (def=0); last target is used for remaining structures")

    parser.add_argument('--gctolerance', type=float, default=5, help="GC tolerance")
    parser.add_argument('--tolerance', type=float, action="append",
                        help="Energy tolerance (def=1); last tolerance is used for remaining structures")


    parser.add_argument('--plot_td', action="store_true",
                        help="Plot tree decomposition; write to treedecomp.pdf")
    
    parser.add_argument('--write_dependency_graph', action="store_true",
                        help="Write the dependency graph; write to dependency_graph.dot and .pdf")


    args=parser.parse_args()

    main(args)
